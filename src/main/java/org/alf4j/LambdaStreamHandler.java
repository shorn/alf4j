package org.alf4j;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.alf4j.log.Log;
import org.alf4j.util.stateful.JsonUtil;
import org.alf4j.util.stateful.JvmUtil;

import java.io.InputStream;

import static org.alf4j.util.stateless.ObjectUtil.infoLogExecutionTime;

/**
 * Use this when you're dealing with parameters that 
 * {@link LambdaRequestHandler} can't deal with (String, and other things that
 * can't be parsed by the {@link RequestHandler} infrastructure.
 */
public abstract class LambdaStreamHandler<I, O>{
  protected Log log = Log.to(this);
  
  protected JvmUtil jvm = new JvmUtil();
  protected JsonUtil json = new JsonUtil();
  
  static {
    Log.staticLog("Lambda class loaded");
  }
  
  
  public abstract O doWork(I input, Context context) throws Exception;
  public abstract I parseInput(InputStream input);
  
  public String handleLambdaInvocation(
    InputStream input, Context context)
  {
    log.info(jvm.describeMemory());

    I event = infoLogExecutionTime(
      log, "parsing input",
      () -> parseInput(input) 
    );
    
    try {
      doWork(event, context);
    }
    catch( Exception e ){
      log.error("error: %s - %s", e, e.getMessage(), json.encodePretty(event));
      throw new RuntimeException(e);
    }
    finally{
      log.info(jvm.describeMemory());
    }
    return "success";
  }
  
  protected void logMemoryStuff(){
    log.info("\nmetaspace - max: %s, used: %s\nruntime - total: %s, free: %s",
      jvm.metaspace().getUsage().getMax(), 
      jvm.metaspace().getUsage().getUsed(), 
      jvm.runtime().totalMemory(), 
      jvm.runtime().freeMemory() );
  }
  
  
}
