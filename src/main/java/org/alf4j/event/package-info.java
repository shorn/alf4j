/**
 * Contains extra "event" classes not provided by the 
 * {@link com.amazonaws.services.lambda.runtime.events} package.
 * <p>
 * These classes follow an "anemic domain model" approach that does nothing but 
 * map to AWS event data structures.  
 * Intended for data-marshalling only, do not put logic in them. 
 */
package org.alf4j.event;