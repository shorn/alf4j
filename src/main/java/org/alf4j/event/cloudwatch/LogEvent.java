package org.alf4j.event.cloudwatch;

/**
<pre>
 "logEvents": [
   {
     "id": "eventId1",
     "timestamp": 1440442987000,
     "message": "[ERROR] First test message"
   },
   {
     "id": "eventId2",
     "timestamp": 1440442987001,
     "message": "[ERROR] Second test message"
   }
 ]
 </pre>
 */
public class LogEvent {
  public String id;
  public String timestamp;
  public String message;
}
