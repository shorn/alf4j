package org.alf4j;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.alf4j.log.Log;
import org.alf4j.util.stateful.JsonUtil;
import org.alf4j.util.stateful.JvmUtil;

import java.util.Map;

/**
 * Use this when taking things that work out of the box with RequestHandler,
 * like {@link org.alf4j.event.cloudwatch.CloudwatchLogsEvent}, otherwise use
 * {@link org.alf4j.LambdaStreamHandler}.
 */
public abstract class LambdaRequestHandler<I, O> 
implements RequestHandler<I, O> {
  static {
    Log.staticLog("Lambda class loaded");
  }
  
  protected Log log = Log.to(this);
  
  protected JvmUtil jvm = new JvmUtil();
  
  public abstract O doWork(I input, Context context, Map<String, String> env)
  throws Exception;
  
  /**
   * This method is used to log the input that caused an exception in the 
   * lambda.
   * <p>
   * The method will setup a whole new JsonUtil object to stringify the input; 
   * if it's the first time that code is used in this lambda, 
   * it may take a while to initialise (up to a few seconds).
   * <p>
   * If you're time, or possibly heap/metaspace restricted, this might be a bad
   * idea.  In that case, override the method and do something that makes 
   * sense in your case.
   */
  protected String debugInput(I input){
    return new JsonUtil().encodePretty(input);
  } 
  
  @Override
  public O handleRequest(
    I input, Context context
  ){
    log.info(jvm.describeMemory());
    O output;
    try {
      output = doWork(input, context, System.getenv());
    }
    catch( Exception e ){
      log.error("error %s - %s", e, e.getMessage(), debugInput(input));
      throw new RuntimeException(e);
    }
    finally{
      log.info(jvm.describeMemory());
    }
    
    return output;
  }
  
}
