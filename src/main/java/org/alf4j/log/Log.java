package org.alf4j.log;


import org.alf4j.log.impl.Log4jLogger;
import org.apache.log4j.Logger;

import java.util.function.Supplier;

public class Log {
  
  public static Log to(Object category){
    if( category == null ){
      return toClass(Log.class);
    }
    
    if( category instanceof Class ){
      return toClass((Class) category);
    }
    else {
      return toInstance(category);
    }
  }
  
  // The plan is that one day this will choose between Log4jLogger or 
  // SystemLogger, in a similar way to how slf4j does it.
  private static Alf4jLogger chooseLogImplementation(Class<?> c){
    return new Log4jLogger(Logger.getLogger(c));
  }
  
  public static Log toClass(Class<?> category){
    return new Log(chooseLogImplementation(category));
  }
  
  /**
   * Creates a category for the class of the given instance.
   */
  public static Log toInstance(Object category){
    return new Log(chooseLogImplementation(category.getClass())); 
  }

  
  private Alf4jLogger log;
  
  private Log(Alf4jLogger log){
    this.log = log;
  }
  
  public void debug(String msg){
    log.debug(msg);
  }
  
  public void debug(Supplier<String> msg){
    if( log.isDebugEnabled() ){
      log.debug(msg.get());
    }
  }

  public void debug(String msg, Object... args){
    if( log.isDebugEnabled() ){
      log.debug(String.format(msg, args));
    }
  }

  public void info(String msg){
    log.info(msg);
  }
  
  public void info(String msg, Object... args){
    if( log.isInfoEnabled() ){
      log.info(String.format(msg, args));
    }     
  }

  public void info(Supplier<String> msg){
    if( log.isInfoEnabled() ){
      log.info(msg.get());
    }
  }

  public void warn(String msg){
    log.warn(msg);
  }
  
  public void warn(String msg, Object... args){
    log.warn(String.format(msg, args));
  }
  
  public void warn(String msg, Throwable t, Object... args){
    log.warn(String.format(msg, args), t);
  }
  
  public void error(String msg){
    log.info(msg);
  }

  public void error(String msg, Object... args){
    log.error(String.format(msg, args));
  }

  public void error(String msg, Throwable t, Object... args){
    log.error(String.format(msg, args), t);
  }

  public RuntimeException runtimeException(String msg, Object... args){
    RuntimeException re = new RuntimeException(String.format(msg, args));
    log.error(re.getMessage());
    return re;
  }
  
  /**
   * This method should only be called from static initialiser blocks.
   * It's intended for doing very little "bootstrap" logging during lambda 
   * creation.
   * <p>
   * Look for logging from static contexts in cloudwatch, 
   * it won't show up in the AWS console when doing test executions.
   */
  public static void staticLog(String msg, Object... args){
    System.out.println(String.format(msg, args));
  }
}
