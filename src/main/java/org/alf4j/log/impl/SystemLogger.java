package org.alf4j.log.impl;

import org.alf4j.log.Alf4jLogger;

import java.util.function.Supplier;

public class SystemLogger implements Alf4jLogger {
  
  private void write(String msg){
    System.out.println(msg);
  }
  
  private void writeThrowable(Throwable t){
    t.printStackTrace();
  }
  
  @Override
  public boolean isDebugEnabled(){
    return true;
  }
  
  @Override
  public void debug(String msg){
    write(msg);
  }
  
  @Override
  public void debug(Supplier<String> msg){
    if( isDebugEnabled() ){
      write(msg.get());
    }
  }
  
  @Override
  public void debug(String msg, Object... args){
    if( isDebugEnabled() ){
      write(String.format(msg, args));
    }
  }
  
  @Override
  public boolean isInfoEnabled(){
    return true;
  }
  
  @Override
  public void info(String msg){
    if( isInfoEnabled() ){
      write(msg);
    }
  }
  
  @Override
  public void info(String msg, Object... args){
    if( isInfoEnabled() ){
      write(String.format(msg, args));
    }
  }
  
  @Override
  public void warn(String msg, Object... args){
    write(String.format(msg, args));
  }
  
  @Override
  public void warn(String msg, Throwable t, Object... args){
    write(String.format(msg, args));
    writeThrowable(t);
  }
  
  @Override
  public void error(String msg){
    write(msg);
  }
  
  @Override
  public void error(String msg, Object... args){
    write(String.format(msg, args));
  }
  
  @Override
  public void error(String msg, Throwable t, Object... args){
    write(String.format(msg, args));
    writeThrowable(t);
  }
  
  
}
