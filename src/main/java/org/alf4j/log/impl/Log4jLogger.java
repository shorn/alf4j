package org.alf4j.log.impl;

import org.alf4j.log.Alf4jLogger;
import org.apache.log4j.Logger;

import java.util.function.Supplier;


public class Log4jLogger implements Alf4jLogger {
  private Logger log;
  
  public Log4jLogger(Logger log){
    this.log = log;
  }
  
  @Override
  public boolean isDebugEnabled(){
    return log.isDebugEnabled();
  }
  
  @Override
  public void debug(String msg){
    log.debug(msg);
  }

  @Override
  public void debug(Supplier<String> msg){
    if( log.isDebugEnabled() ){
      log.debug(msg.get());
    }
  }

  @Override
  public void debug(String msg, Object... args){
    if( log.isDebugEnabled() ){
      log.debug(String.format(msg, args));
    }
  }
  
  @Override
  public boolean isInfoEnabled(){
    return log.isInfoEnabled();
  }
  
  @Override
  public void info(String msg){
    log.info(msg);
  }

  @Override
  public void info(String msg, Object... args){
    if( log.isInfoEnabled() ){
      log.info(String.format(msg, args));
    }     
  }

  @Override
  public void warn(String msg, Object... args){
    log.warn(String.format(msg, args));
  }
  
  @Override
  public void warn(String msg, Throwable t, Object... args){
    log.warn(String.format(msg, args), t);
  }
  
  @Override
  public void error(String msg){
    log.info(msg);
  }

  @Override
  public void error(String msg, Object... args){
    log.error(String.format(msg, args));
  }

  @Override
  public void error(String msg, Throwable t, Object... args){
    log.error(String.format(msg, args), t);
  }

}
