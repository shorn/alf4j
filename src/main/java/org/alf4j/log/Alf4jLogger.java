package org.alf4j.log;

import java.util.function.Supplier;

public interface Alf4jLogger {
  
  boolean isDebugEnabled();
  
  void debug(String msg);
  
  void debug(Supplier<String> msg);

  void debug(String msg, Object... args);

  
  boolean isInfoEnabled();
  
  void info(String msg);
  
  void info(String msg, Object... args);
  
  void warn(String msg, Object... args);

  void warn(String msg, Throwable t, Object... args);
  
  void error(String msg);
    
  void error(String msg, Object... args);

  void error(String msg, Throwable t, Object... args);
  
}
