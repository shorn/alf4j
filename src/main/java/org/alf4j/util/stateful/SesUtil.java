package org.alf4j.util.stateful;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;
import org.apache.commons.codec.Charsets;

import java.nio.ByteBuffer;

public class SesUtil {
  
  AmazonSimpleEmailService ses =
    AmazonSimpleEmailServiceClientBuilder.defaultClient();

  public SendRawEmailResult sendRawEmail(ByteBuffer content){
    SendRawEmailRequest sesRequest = new SendRawEmailRequest().
      withRawMessage(new RawMessage().withData(content));
      
    return ses.sendRawEmail(sesRequest);
    
  }
  
  public SendRawEmailResult sendRawEmail(String content){
    return sendRawEmail(Charsets.UTF_8.encode(content));
  }
  
}
