package org.alf4j.util.stateful;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import org.alf4j.util.stateless.IoUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class S3Util {
  
  private AmazonS3 s3Client = AmazonS3ClientBuilder.standard().build();
  
  public S3Object getObject(
    String bucket,
    String key)
  {
    GetObjectRequest getRequest = new GetObjectRequest(bucket, key);

    return s3Client.getObject(getRequest);
  }

  // http://stackoverflow.com/a/34445210/924597 instead?
  private File getFile(
    String bucket,
    String key,
    File resultDir,
    String resultFileName)
  {
    GetObjectRequest getRequest = new GetObjectRequest(bucket, key);

    S3Object s3Object = s3Client.getObject(getRequest);
    try {
      File resultFile = new File(resultDir, resultFileName);
  
      FileOutputStream resultOutStream = null;
      try{
        resultOutStream = new FileOutputStream(resultFile);
      }
      catch( FileNotFoundException e ){
        throw new RuntimeException(e);
      }
  
      try{
        S3ObjectInputStream is = s3Object.getObjectContent();
        IOUtils.copy(is, resultOutStream);
        IOUtils.closeQuietly(is, null);
      }
      catch( IOException e ){
        throw new RuntimeException(e);
      }

      return resultFile;

    }
    finally {
      IoUtil.closeQuietly(s3Object);
    }
  }
  
}
