package org.alf4j.util.stateful;

import com.amazonaws.util.IOUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ZipUtil {
  // I'm just assuming this is going to need state eventually
  
  public String inflate(byte[] gzippedData){
    GZIPInputStream gis;
    try{
      gis = new GZIPInputStream(new ByteArrayInputStream(gzippedData));
    }
    catch( Exception e ){
      throw new IllegalArgumentException(
        "could not create GZIP input stream on zipped data", e);
    }
    
    BufferedReader bf = new BufferedReader(new InputStreamReader(gis));
    String outStr = "";
    String line;
    
    try{
      while( (line = bf.readLine()) != null ){
        outStr += line;
      }
    }
    catch( Exception e ){
      throw new IllegalArgumentException("could not decompress content", e);
    }
    finally {
      IOUtils.closeQuietly(gis, null);
    }
    
    return outStr;
    
  }

  // I took this from 
  //   http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/JavaDocumentAPIBinaryTypeExample.html
  // Should analyse and see the inflate method should be rewritten 
  public byte[] deflate(String input) throws IOException{
    // Compress the UTF-8 encoded String into a byte[]
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    GZIPOutputStream os = new GZIPOutputStream(baos);
    os.write(input.getBytes("UTF-8"));
    os.close();
    baos.close();
    byte[] compressedBytes = baos.toByteArray();

    // The following code writes the compressed bytes to a ByteBuffer.
    // A simpler way to do this is by simply calling ByteBuffer.wrap(compressedBytes);  
    // However, the longer form below shows the importance of resetting the position of the buffer 
    // back to the beginning of the buffer if you are writing bytes directly to it, since the SDK 
    // will consider only the bytes after the current position when sending data to DynamoDB.  
    // Using the "wrap" method automatically resets the position to zero.
    ByteBuffer buffer = ByteBuffer.allocate(compressedBytes.length);
    buffer.put(compressedBytes, 0, compressedBytes.length);
    buffer.position(0);
    // Important: reset the position of the ByteBuffer to the beginning
    return buffer.array();
  }
  
  
}
