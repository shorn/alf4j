package org.alf4j.util.stateful;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.alf4j.log.Log;
import org.alf4j.util.stateless.IoUtil;
import software.amazon.ion.IonReader;
import software.amazon.ion.IonSystem;
import software.amazon.ion.IonWriter;
import software.amazon.ion.system.IonSystemBuilder;
import software.amazon.ion.system.IonTextWriterBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class JsonUtil {
  private static Log log = Log.to(JsonUtil.class);
  
  private IonSystem ion = IonSystemBuilder.standard().build();

  private ObjectMapper objectMapper;
  private ObjectWriter writer;
  private ObjectWriter prettyWriter;
  
  
  public JsonUtil(){
    objectMapper  = new ObjectMapper();
    objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
    objectMapper.configure(
      DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
    objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    writer = objectMapper.writer();
    prettyWriter = objectMapper.writerWithDefaultPrettyPrinter();
    
  }
  
  
  
  private void rewrite(String textIon, IonWriter writer) {
    IonReader reader = ion.newReader(textIon);
    try{
      writer.writeValues(reader);
    }
    catch( IOException e ){
      throw new RuntimeException(e);
    }
  }

  public String prettyPrint(String json){
    StringBuilder stringBuilder = new StringBuilder();
    try{
      try(
        IonWriter prettyWriter = IonTextWriterBuilder.json().pretty().
          build(stringBuilder)
      ){
        rewrite(json, prettyWriter);
      }
    }
    catch( IOException e ){
      throw new RuntimeException(e);
    }
  
    return stringBuilder.toString();
  }
  
  public String compactPrint(String json){
    StringBuilder stringBuilder = new StringBuilder();
    try{
      try(
        IonWriter prettyWriter = IonTextWriterBuilder.json().
          build(stringBuilder)
      ){
        rewrite(json, prettyWriter);
      }
    }
    catch( IOException e ){
      throw new RuntimeException(e);
    }
  
    return stringBuilder.toString();
  }
  
  
  public String encodePretty(Object o){
    return toJsonPrettyString(o);
  }
  
  public String encodeCompact(Object o){
    return toJsonString(o);
  }
  
  public Map<String, String> decodeStringMap(String json)  {
    return decode(json, new TypeReference<Map<String,String>>() { });
  }
  
  public <T> T decode(String json, TypeReference<T> type)  {
    T object;
    try{
      object = objectMapper.readValue(json, type );
    }
    catch( IOException e ){
      throw new IllegalStateException(String.format(
        "could not decode %s from json: %s", type.toString(), json));
    }
  
    return object;
  }
  
  public <T> T decode(String json, Class<T> type)  {
    T object;
    try{
      object = objectMapper.readValue(json, type );
    }
    catch( IOException e ){
      throw new IllegalStateException(String.format(
        "could not decode %s from json: %s", type.toString(), json));
    }
  
    return object;
  }
  
  public <T> T decode(InputStream jsonStream, Class<T> type)  {
    T object;
    try{
      object = objectMapper.readValue(jsonStream, type );
    }
    catch( IOException e ){
      String jsonString = IoUtil.convertToString(jsonStream);
      log.error("could not decode %s from json: %s", e, 
        type.toString(), jsonString );
      throw new RuntimeException(e);
    }
  
    return object;
  }
  
  private String toJsonPrettyString(Object value){
    try{
      return prettyWriter.writeValueAsString(value);
    }
    catch( Exception e ){
      throw new IllegalStateException(e);
    }
  }
  
  private String toJsonString(Object value){
    try{
      return writer.writeValueAsString(value);
    }
    catch( Exception e ){
      throw new IllegalStateException(e);
    }
  }
  
}
