package org.alf4j.util.stateful;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;

public class JvmUtil {
  private MemoryPoolMXBean metaspace;
  
  
  public MemoryPoolMXBean metaspace(){
    if( metaspace != null ){
      return metaspace;
    }
    
    for( MemoryPoolMXBean memoryMXBean : ManagementFactory.getMemoryPoolMXBeans() ){
      if( "Metaspace".equals(memoryMXBean.getName()) ){
        metaspace = memoryMXBean;
      }
    }    
    if( metaspace == null ){
      throw new IllegalStateException("could not find Metaspace Memory bean");
    }
    return metaspace;
  }
  
  public long getUsedMetaspace(){
    for( MemoryPoolMXBean memoryMXBean : ManagementFactory.getMemoryPoolMXBeans() ){
      if( "Metaspace".equals(memoryMXBean.getName()) ){
        return memoryMXBean.getUsage().getUsed();
      }
    }    
    throw new IllegalStateException("couldn't fine metaspace bean");    
  }
  
  public long getMaxMetaspace(){
    for( MemoryPoolMXBean memoryMXBean : ManagementFactory.getMemoryPoolMXBeans() ){
      if( "Metaspace".equals(memoryMXBean.getName()) ){
        return memoryMXBean.getUsage().getMax();
      }
    }    
    throw new IllegalStateException("couldn't fine metaspace bean");    
  }
  
  public Runtime runtime(){
    return Runtime.getRuntime();
  }
  
  public String describeMemory(){
    return String.format(
      "metaspace - max: %s, used: %s\nruntime - total: %s, free: %s",
      metaspace().getUsage().getMax(), 
      metaspace().getUsage().getUsed(), 
      runtime().totalMemory(), 
      runtime().freeMemory() );
  }
  
}
