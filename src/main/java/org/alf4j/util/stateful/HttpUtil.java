package org.alf4j.util.stateful;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class HttpUtil {
  
  HttpClient client = HttpClientBuilder.create().build();
  
  public HttpResponse execute(HttpUriRequest request){
    try{
      return client.execute(request);
    }
    catch( IOException e ){
      throw new RuntimeException(e);
    }
  }
  
}
