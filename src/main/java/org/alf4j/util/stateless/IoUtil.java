package org.alf4j.util.stateless;

import com.amazonaws.util.IOUtils;
import org.alf4j.log.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IoUtil {
  private static org.alf4j.log.Log LOG = Log.to(IoUtil.class);
  
  /**
   * "quietly" refers to not throwing exceptions, the exception will be 
   * logged as an error.
   */
  public static void closeQuietly(@Nullable Closeable is, @Nullable Log log){
    if( is == null ){
      return;
    }
    
    try{
      is.close();
    }
    catch( Exception ex ){
      Log logger = log == null ? LOG : log;
      logger.error("Ignore failure in closing the Closeable", ex);
    }
  }
  
  /** delegates to @(link {@link #closeQuietly(Closeable, Log)} */
  public static void closeQuietly(@Nullable Closeable is){
    closeQuietly(is, null);
  }

  public static long copy(InputStream in, OutputStream out){
    try{
      return IOUtils.copy(in, out);
    }
    catch( IOException e ){
      throw new RuntimeException(e);
    }
  }
  
  public static String convertToString(InputStream input){
    try{
      return IOUtils.toString(input);
    }
    catch( IOException e ){
      throw new RuntimeException(e);
    }
  }
  
  
}
