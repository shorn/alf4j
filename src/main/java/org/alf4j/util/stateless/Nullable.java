package org.alf4j.util.stateless;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a field or parameter may be null.
 * Absence of this anno implies a field or param is not allowed to be null.
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
// do we really need/want it at runtime?
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Nullable {
}