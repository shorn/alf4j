package org.alf4j.util.stateless;

public class Check {
  
  public static void areEqual(
    @Nullable String left,
    @Nullable String right)
  {
    if( !StringUtil.areEqual(left, right) ){
      throw new IllegalArgumentException(
        "`" + left + "` not equal to `" + right + "`" );
    }
  }
  

  /**
   * Null is not a value.  Empty string is not a value.  Whitespace is not a
   * value.
   */
  public static void hasValue(String msg, @Nullable String target) {
    if( StringUtil.isBlank(target) ){
      throw new IllegalArgumentException(msg);
    }
  }
  
  public static void hasValue(@Nullable String target){
    hasValue("target has no value", target);
  }

  
}
