package org.alf4j.util.stateless;


import org.alf4j.log.Log;

import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class ObjectUtil {
  private static Log log = Log.to(ObjectUtil.class);
  
  public static long parseLong(
    @Nullable String value,
    long defaultValue)
  {
    try{
      return Long.parseLong(value);
    }
    catch( NumberFormatException e ){
      return defaultValue;
    }
  }
  
  /**
   * This will execute the given function, but "pad out" the execution of this 
   * method so that overall execution takes at least delayMillis.
   * Does nothing if execution time > delay 
   * (this method is not about "timeout" functionality).
   */
  public static <T> T padExecution(long delayMillis, Supplier<T> supplier){
    long start = System.currentTimeMillis();
    try {
      return supplier.get();
    }
    finally {
      long execMs = System.currentTimeMillis() - start;
      
      if( delayMillis <= 0 ){
        log.debug("no delay - < 0 delay of %s", delayMillis);
      }
      else {
        log.debug("execDelay: %s", delayMillis);
        long timeLeftMs = delayMillis - execMs;
        
        if( timeLeftMs > 0 ){
          
          try{
            log.warn("execution delay for %s", timeLeftMs);
            Thread.sleep(timeLeftMs);
          }
          catch( InterruptedException e ){
          }
          log.debug("execution resumed");

        }
        else {
          log.debug("no delay, execution already took %s", execMs);
        }
      }
    }
  }

  // put supplier last so that multi-line lambda's read better
  public static <T> T infoLogExecutionTime(
    Log log, String description, Supplier<T> r
  ) {
    return timeExecution( 
      r,
      (time, __) -> log.info("%s took %s ms", description, time) 
    );
  }
  
  public static <T> T timeExecution(
    Supplier<T> r, 
    BiConsumer<Long, T> handler 
  ) {
    long beforeReq = System.nanoTime();
    T result;
    try{
      result = r.get();
    }
    catch( Exception e ){
      throw new RuntimeException(e);
    }
    long time = System.nanoTime() - beforeReq;
    handler.accept(time / 1_000_000, result);
    return result;
  }
  
}
