package org.alf4j.util.stateless;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public class StreamUtil {
  
  /**
   * This collector will throw an exception if isn't used on a stream
   * with exactly one element.
   */
  public static <T> Collector<T, List<T>, T> singletonCollector(
    String msg
  ){
    return Collector.of(
      ArrayList::new,
      List::add,
      (left, right) -> {
        left.addAll(right);
        return left;
      },
      list -> {
        if( list.size() != 1 ){
          throw new IllegalStateException(msg);
        }
        return list.get(0);
      }
    );
  }
  
  public static <T> Collector<T, List<T>, T> singletonCollector(){
    return singletonCollector("");
  }  
}
