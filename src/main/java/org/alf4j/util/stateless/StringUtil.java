package org.alf4j.util.stateless;

import java.util.Collection;
import java.util.stream.Collectors;

public class StringUtil {
  
 /**
   * @return true if both values are null
   */
  @SuppressWarnings("StringEquality")
  public static boolean areEqual(
    @Nullable String l,
    @Nullable String r)
  {
    // shortcut deal with instance equality and two null values
    if( l == r ){
      return true;
    }
    if( l == null || r == null ){
      return false;
    }

    return l.equals(r);
  }
  
  /**
   * looks only for null or empty, " " or other whitespace will return false.
   */
  public static boolean isNullOrEmpty(@Nullable String string){
    return string == null || string.length() == 0; 
  }
  
  public static boolean isWhitespace(@Nullable CharSequence seq){
    if( seq == null ){
      return false;
    }
  
    int sz = seq.length();
    for( int i = 0; i < sz; ++i ){
      if( !Character.isWhitespace(seq.charAt(i)) ){
        return false;
      }
    }
  
    return true;
  }

  /**
   * Null is not a value.  Empty string is not a value.  Whitespace is not a
   * value.
   */
  public static boolean hasValue(String target) {
    return !isBlank(target);
  }

  /**
   * Null is blank. Empty string is blank.  All whitespace is blank.
   */
  public static boolean isBlank(String target) {
    if( StringUtil.isNullOrEmpty(target) ){
      return true;
    }
    return isWhitespace(target);
  }
  
  public static String format(@Nullable Collection<?> c){
    if( c == null ){
      return "";
    }
    return format(c, ", ");
  }
  
  public static String format(@Nullable Collection<?> c, String separator){
    if( c == null ){
      return "";
    }
    return c.stream().
      map(i -> i == null ? "null" : i.toString()).
      collect(Collectors.joining(separator));
  }
  
}
