package org.alf4j.log;

import junit.framework.TestCase;

/**
 I wasn't sure if having methods with both varargs and throwable was going to 
 resolve correctly at runtime.
 */
public class LogTest extends TestCase{
  
  public static void println(String s){
    System.out.println(s);
  }
  public void testIt(){
    ParamThing thing = new ParamThing();
    thing.warn("1", new RuntimeException("bleah"));
    thing.warn("2", new RuntimeException("bleah"), 42);
    thing.warn(new RuntimeException("bleah"), "3", 42);
    thing.warn(new RuntimeException("bleah"), "4");
    thing.warn("5", "bleah");
    thing.warn("6");
  }
  
  public static class ParamThing {
    public void warn(String msg){
      println("single param string method called for: " + msg);
    }
    
    public void warn(String msg, Object... args){
      println("varargs method called for: " + msg);
    }

    public void warn(Throwable t, String msg, Object... args){
      println("varargs throwable method called for: " + msg);
    }

//    public void warn(String msg, Throwable t){
//      println("throwable method called");
//    }
  }
}


